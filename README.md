# JWT Api

_APIS elaborado en .Net Core en su versión 3.1. la estructura básica está en el único controlador que tiene el servicio y en sus modelos se encuentra la la lógica para la validacion de los client y secrets como la generación del jwt en un RS256_

## Información General
- Proyecto de Laboratorio Software Avanzado
- Creado por Haroldo Arias
- Carnet 201020247
- Octubre 2020


## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Instalación 🔧

_La instalación es muy sencilla, si tienes visual studio en su versión 2019 solo basta con abrirla con el IDE y ejecutarla, caso contrario te explicó el procedimiento_

### Pre-requisitos 📋
_Debes de tener instalado el SDK del .NetCore en su versión 3.1_
* [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1) - Acá puedes descargarlo

## Compilación del Servicio ⚙️
_Tienes que ingresar en cada una de las carpetas por separado, Si se utiliza el IDE del Visual Studio solo basta con Compilarlo dentro del IDE_
Manualmente hay que correr el siguiente comando
```
dotnet build
``` 

## Ejecución del Servicio
_Si se utiliza el IDE del Visual Studio solo basta con Ejecutarlo dentro del IDE_
Manualmente hay que correr el siguiente comando

_Servicio de Clientes, correrá en el puerto 5000_

```
dotnet run --project=jwt_api
``` 

## Autor ✒️

* **Haroldo Pablo Arias Molina** - *Trabajo Inicial* - [harias25](https://github.com/harias25)
 
## Licencia 📄

Este proyecto está bajo la Licencia Libre - mira el archivo [LICENSE.md](LICENSE.md) para detalles