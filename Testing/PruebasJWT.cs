using jwt_api.Models;
using System;
using System.IO;
using Xunit;

namespace Testing
{
    public class PruebasJWT
    {
        [Fact]
        public void validarParametros()
        {
            DotNetEnv.Env.Load("/cicd/microserviciojwt/.env");
            Console.WriteLine(DotNetEnv.Env.GetString("DB_USER_TEST", "NO SE OBTUVO EL KEY DEL .env TEST valid"));
            Client client = new Client(DotNetEnv.Env.GetString("CLIENT_TEST", "parchis"), DotNetEnv.Env.GetString("SECRET_TEST", "gjq0JaeBPi"), DotNetEnv.Env.GetString("DB_USER_TEST", "jwt_api_test"));
            Assert.True(client.IsValidClient());
        }

        [Fact]
        public void generaToken()
        {
            DotNetEnv.Env.Load("/cicd/microserviciojwt/.env");
            Console.WriteLine(DotNetEnv.Env.GetString("DB_USER_TEST", "NO SE OBTUVO EL KEY DEL .env Test generar"));
            Client client = new Client(DotNetEnv.Env.GetString("CLIENT_TEST", "parchis"), DotNetEnv.Env.GetString("SECRET_TEST", "gjq0JaeBPi"), DotNetEnv.Env.GetString("DB_USER_TEST", "jwt_api_test"));
            Exception ex;
            Assert.NotNull(client.GetAccessToken(out ex));
        }
    }
}
