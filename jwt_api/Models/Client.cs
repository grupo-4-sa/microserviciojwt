﻿using Conexiones;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;

namespace jwt_api.Models
{
    public class Client
    {
        private readonly String client_id;
        private readonly String client_secret;
        private readonly IConexion conn;

        /// <summary>
        /// Constructor de la clase Client
        /// </summary>
        /// <param name="client_id">valor del id del cliente que necesita generar un token</param>
        ///  <param name="client_secret">valor del secret del cliente que necesita generar un token</param>
        ///  <param name="tagConexion">tag para saber apertura la conexion de la base de datos</param>
        public Client(String client_id, String client_secret, String tagConexion)
        {
            this.client_id = client_id;
            this.client_secret = client_secret;
            ConexionDB connDB = new ConexionDB(tagConexion);
            conn = connDB.obtenerConexion();
        }


        /// <summary>
        /// Verifica si el client y secret son validos y existen en nuestra base de datos
        /// </summary>
        /// <returns> retorna true si es valido o false caso contrario </returns>
        public bool IsValidClient()
        {
            if (client_id == "" || client_id == null)
                return false;
            if (client_secret == "" || client_secret == null)
                return false;

            int count = int.Parse(conn.getSimpleSelect("select count(1) from jwt_client where Client='" + this.client_id + "' and Secret='" + this.client_secret + "';", "jwt").ToString());

            if (count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Genera un token basado en el client ingresado, retornara un String con el response del json a enviar.
        /// </summary>
        /// <param name="error">Variable Exception que se cargara unicamente cuando exista una excepción</param>
        /// <returns>String con la estructura del Json a retornar</returns>
        public String GetAccessToken(out Exception error)
        {
            try
            {
                DotNetEnv.Env.Load("../.env");
                Console.WriteLine("");
                Console.WriteLine(DotNetEnv.Env.GetString("PUBLIC_KEY", "NO SE OBTUVO EL PUBLIC_KEY DEL .env"));
                String publicKey = DotNetEnv.Env.GetString("PUBLIC_KEY", "<RSAKeyValue><Modulus>zfjbU9kH4d8jWUMc3u5h7CfbxqVCMxIbNFwrb96RwpEzGy47KqB6el/tjKZPlSz9AcAGDxDY0MzvmtadFXD5BmocAzCMoWQ9VpP6V9tMA8xKwZJejiildE2j40dhl+CuL8hurlf36byPjM6M9V/bMKjTQzbS09B0JwLOMMFj81QqkO6iSTfyoxAcSti9uwa27SEJt9OjYz9aVXSopaIEA+xuHbMLQQcjJsjn2Fg7f7oLQmBz+tQjwYbwjS0f/4z64tqvUCqTZTxii4wEpId1ws6VQ68Y3CUlojRbSDgBmsmLdwcmwoKHOXDa2l0lJjS03NiTWsiOhBkv784NULXZoQ==</Modulus><Exponent>AQAB</Exponent><P>33qD4AJ1ijo0jfvz4A/PdLWP+Vn7IIs012L408OmugtxzE6Mnoa5IOLbgtuSA8vjdjnZvcpU9m9/OfdHWrwd6oYvYpSRBK/sFBz0VcC5r2lxvLP1SmWDLwN+DsKiykUXzLQc+8TrpIX5FzE7YbqZLkCnGI4ez/HHbOMELQh5BSM=</P><Q>6/IneCZeiod4+3LVzvE8NOgf0XHfpXfUlhjDu9QJwzMXo1nAgIMftVC66TQBgwbsUFhTgkiZIFRp1DUVDMe9QHoV3luVRCrsRzRSn/7OwqH4ugKgUobCGfqXJp97IsqQ66BRlJB015OJaI/C6Z7pE88dkUs90AgXIOlufjlpvGs=</Q><DP>iHrx/uSBlCFn5y5wCe6d69DWVJo3W/CoHMI85Fcgg45puTRffJEajpL9LDWcPuVNzO4XpGjk6nu9X1E9KpSoUIoSdkn1Jr+plb75GUXSIOZsp6nfq8fDDfs8CRelA5Kerk1xRfFFV/zZ2ugeM9kZo72oTr684pHuEPmzmy0Je7E=</DP><DQ>TMVB6moxoMlrqAU5ZFhBysUsdS2kI2bWGPB934OimYzqSBBP/oCrtIdMCc0OIOADzDCxG1cb29UbihyreLkU7wHKU8tgygldT93KdJyoEdQ55ews1i1awrEytTJ3El7uwHwtjzVhjzNGJmuAdBtsozFk4X2l4yZ+wAq0FQYBTXk=</DQ><InverseQ>lMdtQN6Q0HZ9QOgloMTJbHCpJ2ruKADtqtcVa/e2Uabg22qG4Et4DXwGJrGNeHeYrDkZMIUzhjhAXBajwZuIXOL4ZL1ENiB1OswDT7MWstdncSnM2+rxuK1DvE8UWYmF1jvQU2FAeYjOZo8BMCjt19oTbkJ+ilwOUZyZtfeO6U0=</InverseQ><D>NBevaOpn+vFTYFz/7UX/EBoYlInxTzdSGBEDj1yMIthUQ5iSsDo8H70pODxLvl/NcWbeYWsSWuSp6NBElEqpQL0PbqMVvi+6762nUBcPh5pggcqCDZQsHFkSjQXdoeraQw1/KKro8X2WFLecoOZoaksYe1RfR+Ddr4wfF+ehXsXDeElocv6u/DTRDcTlJZZgDGqIzmd3ncClsX7FH2ipfDyIPKrVbjNQlOV8tw9A3sa1LpcUlp7xRCdtPK0tK2OSPy0p6DDNpWidB1i15qa0iupQa8E5Pjaw9/VEwYhxD6rymzpaOSm2BJJlCeqaTRIIxOM3WFsyzlT78tNG3pb27Q==</D></RSAKeyValue>");
                String scopes = "[";
                List<String> list = new List<string>();
                DataTable datos = conn.getTableByQuery("select RutaKey from jwt_access_client where Client='" + this.client_id + "';", "jwt");
                foreach(DataRow row in datos.Rows)
                {
                    list.Add(row[0].ToString());

                }

                scopes += string.Join(",", list) + "]";

                Console.WriteLine("");
                Console.WriteLine("Scopes con Accesos: " + scopes);

                var rsaParams = new RSACryptoServiceProvider();
                rsaParams.FromXmlString(publicKey);
                var rsaSecurityKey = new RsaSecurityKey(rsaParams);
                var signingCredentials = new SigningCredentials(rsaSecurityKey, SecurityAlgorithms.RsaSha256);

                String token = string.Empty;

                var claim = new[]
                {
                    new Claim("alg", "RS256"),
                    new Claim("typ",  "JWT"),
                    new Claim("exp",  "3600"),
                    new Claim("scope", scopes),
                };

                var jwtToken = new JwtSecurityToken(
                    issuer:"localhost:5000/token",
                    audience:"localhost:5000/token",
                    claims:claim,
                    expires: DateTime.Now.AddSeconds(3600),
                    signingCredentials: signingCredentials
                );
                token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
                error = null;
                return JsonConvert.SerializeObject(new Token(token));
            }
            catch(Exception ex){
                Console.WriteLine("");
                Console.WriteLine("*************************************Error en Client - GetAccessToken **********************************");
                Console.WriteLine(ex);
                error = ex;
                return null;
            }
        }
    }
}
