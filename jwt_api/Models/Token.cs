﻿using System;

namespace jwt_api.Models
{

    /// <summary>
    /// Clase principal para el parseo y el retorno del token jwt que se devolvera al cliente cuando los datos sean exitosos
    /// </summary>
    public class Token
    {
        public String jwt { get; set; }

        /// <summary>
        /// Constructor de la clase
        /// </summary>
        /// <param name="jwt">String del token generado previamente para ser almacenado</param>
        public Token(String jwt)
        {
            this.jwt = jwt;
        }
    }
}
