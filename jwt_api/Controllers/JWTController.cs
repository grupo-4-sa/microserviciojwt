﻿using System;
using System.Collections.Generic;
using jwt_api.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace jwt_api.Controllers
{

    [ApiController]
    public class JwtController : Controller
    {
        /// <summary>
        /// Metodo POST principal para la validacion de los parametros y su creación del token Bearer 
        /// </summary>
        /// <param name="id">Id del Cliente</param>
        /// <param name="secret">Secret del Cliente</param>
        /// <returns>Retorna el Json con la estructura establicida si fuese exitoso el ingreso de paramestros</returns>
        [Route("token")]
        [HttpPost]
        public ActionResult<IEnumerable<String>> Post(String id, String secret)
        {
            DotNetEnv.Env.Load("../.env");
            //Console.WriteLine(DotNetEnv.Env.GetString("DB_USER_PROD", "NO SE OBTUVO EL KEY DEL .env"));
            Console.WriteLine("");
            Console.WriteLine("Cliente " + id + " y Secret " + secret + " solicitan JWT");
            
            Client client = new Client(id, secret, DotNetEnv.Env.GetString("DB_USER_PROD", "jwt_user"));

            if (!client.IsValidClient())
            {
                Console.WriteLine("");
                Console.WriteLine("Cliente " + id + " y Secret " + secret + " no existen!!!");
                return BadRequest();
            }

            Exception error;
            String token = client.GetAccessToken(out error);

            if (token != null)
            {
                Console.WriteLine("");
                Console.WriteLine("Token Generado:  " + token);
                return Content(token, "application/json");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("Token No Generado por Error:  " + error.Message);
                return BadRequest(JsonConvert.SerializeObject(error));
            }

        }
    }
}
